<?php

use Phinx\Migration\AbstractMigration;

class CreateProxyEntity extends AbstractMigration
{
    private $tableName = 'proxy_entity';

    public function up()
    {
        $proxy = $this->table($this->tableName);
        $proxy->addColumn('host', 'string')
            ->addColumn('port', 'string')
            ->addIndex(['host', 'port'], [
                    'unique' => true,
                    'name' => "idx_{$this->tableName}_host_port"])
            ->addColumn('quality', 'enum', [
                'values' => ['GOOD', 'BAD', 'DEAD'],
                'default' => 'BAD'
            ])
            ->addColumn('timeout', 'integer', ['default' => 20])
            ->addColumn('lastCheckTime', 'integer', ['default' => null])
            ->addColumn('lastRequestTime', 'integer', ['default' => null])
            ->save()
        ;
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
