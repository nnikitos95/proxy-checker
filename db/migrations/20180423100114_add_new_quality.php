<?php


use Phinx\Migration\AbstractMigration;

class AddNewQuality extends AbstractMigration
{
    private $table = 'proxy_entity';

    public function up()
    {
        $this->table($this->table)
            ->changeColumn('quality', 'enum', [
                'values' => ['GOOD', 'BAD', 'DEAD', 'REPEAT'],
                'default' => 'BAD'
            ])
            ->update()
        ;
    }

    public function down()
    {
        $this->query("DELETE FROM $this->table WHERE quality = 'REPEAT'");
        $this->table($this->table)
            ->changeColumn('quality', 'enum', [
                'values' => ['GOOD', 'BAD', 'DEAD'],
                'default' => 'BAD'
            ])
            ->update()
        ;
    }
}
