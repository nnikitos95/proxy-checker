<?php

use App\models\proxy\ProxyEntity;
use Phinx\Migration\AbstractMigration;

class MigrateTimeoutIfItGreaterThanNewMaxTimeout extends AbstractMigration
{
    private $table = 'proxy_entity';
    private $column = 'timeout';

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $timeout = ProxyEntity::DEFAULT_TIMEOUT;
        $this->execute("
            UPDATE {$this->table}
            SET {$this->column} = $timeout
            WHERE {$this->column} > $timeout
        ");
    }
}
