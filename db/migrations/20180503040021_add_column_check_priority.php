<?php

use App\models\proxy\ProxyEntity;
use Phinx\Migration\AbstractMigration;

class AddColumnCheckPriority extends AbstractMigration
{
    private $table = 'proxy_entity';
    private $column = 'checkPriority';

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table($this->table)
            ->addColumn($this->column, 'enum', [
                'values' => [ProxyEntity::CHECK_PRIORITY_LOW, ProxyEntity::CHECK_PRIORITY_HIGH],
                'default' => ProxyEntity::CHECK_PRIORITY_LOW
            ])
            ->update()
        ;
    }
}
