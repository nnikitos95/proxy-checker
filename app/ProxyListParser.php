<?php

namespace App;

class ProxyListParser
{
    /**
     * @var string
     */
    protected $strList;

    public function __construct(string $strList)
    {
        $this->strList = $strList;
    }

    /**
     * @return array
     */
    public function parse()
    {
        $sep = "\n";
        if (preg_match("/(\\r\\n|\\n|\\r)/", $this->strList, $arrSep)) {
            $sep = $arrSep[0];
        }
        $arr = explode($sep, $this->strList);
        $newArr = [];
        foreach ($arr as $elem) {
            $urlItems = parse_url($elem);
            if (preg_match('/socks[5|4]/', $urlItems['scheme'])) {
                $urlItems['scheme'] = 'https';
            }

            $urlItems['type'] = $urlItems['scheme'];
            unset($urlItems['scheme']);
            $newArr[] = $urlItems;
        }

        return $newArr;
    }
}