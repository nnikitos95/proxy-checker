<?php

namespace App;

class Config
{
    private static $config = [];

    private function __construct()
    {}

    private function __clone()
    {}

    public static function init(array $config)
    {
        self::$config = $config;
    }

    /**
     * @param $option
     * @return array
     */
    public static function getOption($option): array
    {
        if (self::hasOption($option)) {
            return self::$config[$option];
        }

        return [];
    }

    /**
     * @param $option
     * @return bool
     */
    public static function hasOption($option): bool
    {
        return isset(self::$config[$option]);
    }

    /**
     * @param $option
     * @param $value
     */
    public static function setOption($option, $value)
    {
        self::$config[$option] = $value;
    }
}