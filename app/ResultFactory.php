<?php

namespace App;

class ResultFactory
{
    public static function createSuccess()
    {
        return new Result(Result::SUCCESS);
    }

    public static function createFail(\Exception $e)
    {
        return new Result(Result::FAIL, $e);
    }
}