<?php

namespace App;

use App\models\adapter\AbstractProxyDataToLoadDataAdapter;
use App\models\provider\AbstractProxyDataProvider;
use App\models\proxy\ProxyEntity;

class ProxyLoader
{
    /**
     * @var int
     */
    protected $similarCount = 0;

    /**
     * @var int
     */
    protected $uniqueCount  = 0;

    /**
     * @var float
     */
    protected $time;

    /**
     * @var AbstractProxyDataProvider
     */
    protected $provider;

    /**
     * @var AbstractProxyDataToLoadDataAdapter
     */
    protected $adapter;

    /**
     * Loader constructor.
     * @param AbstractProxyDataProvider $provider
     * @param AbstractProxyDataToLoadDataAdapter $adapter
     */
    public function __construct(AbstractProxyDataProvider $provider, AbstractProxyDataToLoadDataAdapter $adapter)
    {
        $this->provider = $provider;
        $this->adapter  = $adapter;
    }

    /**
     * @return void
     */
    public function load()
    {
        $begin = microtime(true);
        $pdo = Registry::getDb();
        $this->provider->downloadData();
        if ($this->provider->hasData()) {
            $dataForLoad = $this->adapter->getData($this->provider);
            while (!empty($dataForLoad)) {
                $splice = $this->getSplice($dataForLoad);
                $count = $this->insert($splice, $pdo);
                $this->uniqueCount += $count;
            }
        }
        $this->calculateCounts();
        $this->time = microtime(true) - $begin;
    }

    /**
     * @param array $rows
     * @param \PDO $pdo
     * @return string
     */
    protected function buildQuery(array $rows, \PDO $pdo)
    {
        $columns = array_keys($rows[0]);
        $sql = "INSERT OR IGNORE INTO " . ProxyEntity::tableName();
        $sql .= "(" . implode(",", $columns) . ") VALUES ";
        $lists = [];
        foreach ($rows as $row) {
            $values = [];
            foreach (array_keys($row) as $column) {
                $values[] = $pdo->quote($row[$column]);
            }
            $lists[] = "(" . implode( ",", $values ) . ")";
        }
        return $sql . implode(",", $lists);
    }

    /**
     * @return void
     */
    protected function calculateCounts()
    {
        $this->similarCount = $this->provider->getCount() - $this->uniqueCount;
    }

    /**
     * @return float
     */
    public function getTime(): float
    {
        return $this->time;
    }

    /**
     * @param array $arrayForSplice
     * @param int $count
     * @return array
     */
    protected function getSplice(array &$arrayForSplice, $count = 1000): array
    {
        return array_splice($arrayForSplice, 0, $count);
    }

    /**
     * @param array $rows
     * @param \PDO $pdo
     * @return int
     */
    protected function insert(array $rows,\PDO $pdo)
    {
        $query = $this->buildQuery($rows, $pdo);
        return $pdo->exec($query);
    }

    /**
     * @return int
     */
    public function getSimilarCount(): int
    {
        return $this->similarCount;
    }

    /**
     * @return int
     */
    public function getUniqueCount(): int
    {
        return $this->uniqueCount;
    }
}