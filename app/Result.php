<?php

namespace App;

use GuzzleHttp\Exception\RequestException;

class Result
{
    const SUCCESS = true;
    const FAIL    = false;

    /**
     * Result constructor.
     * @param bool $result
     * @param \Exception $exception
     */
    public function __construct(bool $result, \Exception $exception = null)
    {
        if ($result == self::SUCCESS) {
            $this->result = $result;
        } elseif ($result == self::FAIL) {
            if ($exception == null) {
                throw new \InvalidArgumentException('Param $exception must be a instance of ' .
                    \Exception::class . ' null given'
                );
            }
            $this->result = $result;
            $this->exception = $exception;
        }
    }

    /**
     * @var bool
     */
    private $result;

    /**
     * @var RequestException
     */
    private $exception;

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->result;
    }

    /**
     * @return \Exception
     */
    public function getException(): \Exception
    {
        return $this->exception;
    }
}