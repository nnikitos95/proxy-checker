<?php

if (!isset($argv[2])) {
    exit("Need filename \n");
}

$testsPath = APP_PATH . '/app/tests/simple/';
$file = $argv[2] . '.php';
$filePath = $testsPath . $file;

if (file_exists($filePath)) {
    require $filePath;
} else {
    exit("file $file is not found in $testsPath \n");
}