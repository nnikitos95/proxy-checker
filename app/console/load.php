<?php

use App\models\BestProxiesLoaderBuilder;
use App\models\ProxyDockerLoaderBuilder;
use App\Registry;

$message = "Source: %s, New: %d, similar: %d, time: %.2f";
$proxyDockerConfig = require APP_PATH . '/config/api/proxy-docker/config.php';

$builder1 = new ProxyDockerLoaderBuilder($proxyDockerConfig);
$loader1 = $builder1->build();
$loader1->load();

Registry::getConsoleLogger()->info(sprintf($message,
    "proxy-docker",
    $loader1->getUniqueCount(),
    $loader1->getSimilarCount(),
    $loader1->getTime())
);

$bestProxiesConfig = require APP_PATH . '/config/api/best-proxies/config.php';

$builder2 = new BestProxiesLoaderBuilder($bestProxiesConfig);
$loader2 = $builder2->build();
$loader2->load();

Registry::getConsoleLogger()->info(sprintf($message,
        "best-proxies",
        $loader2->getUniqueCount(),
        $loader2->getSimilarCount(),
        $loader2->getTime())
);
exit();