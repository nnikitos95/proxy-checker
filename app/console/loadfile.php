<?php

use App\models\ProxyFromFileLoaderBuilder;
use App\Registry;

$path = $argv[2] ?? null;
if (!$path) {
    exit('Need argument' . PHP_EOL);
}

if (!file_exists($path)) {
    exit('File is not found' . PHP_EOL);
}

$builder = new ProxyFromFileLoaderBuilder($path);
$loader = $builder->build();
$loader->load();

$message = "Source: %s, New: %d, similar: %d, time: %.2f";

Registry::getConsoleLogger()->info(sprintf($message,
        "From file",
        $loader->getUniqueCount(),
        $loader->getSimilarCount(),
        $loader->getTime())
);