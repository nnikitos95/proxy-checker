<?php

use App\models\adapter\ProxyEntityToProxyEntityCheckContextAdapter;
use App\models\adapter\RowToProxyEntityAdapter;
use App\models\checker\ProxyChecker;
use App\models\event\ProxyEntityCheckEvent;
use App\models\formatter\ProxyEntityCheckEventResultFormatter;
use App\models\proxy\ProxyEntity;
use App\models\storage\UrlStorage;
use App\Registry;

$limit = 300;
$minLimit = 50;
$param = $argv[2] ?? $limit;

if ($param && (int) $param > $minLimit) {
    $limit = $param;
}

$states = [
    ProxyEntity::BAD,
    ProxyEntity::REPEAT,
    ProxyEntity::GOOD
];

$uriList = require APP_PATH . '/config/links-default.php';
$urlStorage = new UrlStorage($uriList);

$formatter = new ProxyEntityCheckEventResultFormatter();
$consoleLogger = Registry::getConsoleLogger();
$fileLogger = Registry::getApiRequestLogger();

$proxyList = [];

while (true) {
    $proxyList = ProxyEntity::getByOrm()
        ->where(['quality' => $states])
        ->orderBy('checkPriority', 'DESC')
        ->orderBy('lastCheckTime')
        ->limit($limit)
        ->fetchAll()
    ;

    if (empty($proxyList)) {
        sleep(60);
        continue;
    }

    $proxyList = (new RowToProxyEntityAdapter())->getMany($proxyList);
    $proxyList = (new ProxyEntityToProxyEntityCheckContextAdapter())->getMany($proxyList, [
        function(ProxyEntityCheckEvent $event) use ($formatter, $consoleLogger, $fileLogger) {
            $message = $formatter->format($event);
            $consoleLogger->info($message);
            $fileLogger->info($message);
        }
    ]);

    (new ProxyChecker($proxyList, $urlStorage))->check();

    $proxyList = [];
}