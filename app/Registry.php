<?php

namespace App;

use Katzgrau\KLogger\Logger;
use LessQL\Database;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Logger\ConsoleLogger;

class Registry
{
    /**
     * @var \PDO
     */
    private static $db;

    /**
     * @var Database
     */
    private static $orm;

    /**
     * @var ConsoleLogger
     */
    private static $apiLogger;

    private static $appLogger;

    private static $consoleLogger;

    private function __construct()
    {}

    public function __clone()
    {}

    /**
     * @return \PDO
     */
    public static function getDb()
    {
        $db = Config::getOption('db');
        if (self::$db == null) {
            self::$db = new \PDO("sqlite:" . $db['path']);
        }
        return self::$db;
    }

    /**
     * @return Database
     */
    public static function getORM()
    {
        $pdo = self::getDb();
        if (self::$orm == null) {
            self::$orm = new Database($pdo);
        }

        if (self::$orm->pdo == null) {
            self::$orm->pdo = $pdo;
        }
        return self::$orm;
    }

    public static function getApiRequestLogger()
    {
        if (self::$apiLogger == null) {
            $logCfg = Config::getOption('log');
            $options = [
                'dateFormat' => 'G:i:s',
                'prefix' => 'result-',
                'logFormat' => "{date} | {message}",
                'extension' => 'log'
            ];
            $logger = new Logger($logCfg['path'] . $logCfg['result'],
                LogLevel::DEBUG, $options);
            self::$apiLogger = $logger;
        }

        return self::$apiLogger;
    }

    public static function getConsoleLogger()
    {
        if (self::$consoleLogger == null) {
            $options = [
                'dateFormat' => 'G:i:s',
                'logFormat' => "{message}",
            ];
            $logger = new Logger('php://STDOUT',
                LogLevel::DEBUG, $options);
            self::$consoleLogger = $logger;
        }

        return self::$consoleLogger;
    }

    public static function getApplicationLogger()
    {
        //TODO поему-то не работает
        if (self::$appLogger == null) {
            $logCfg = Config::getOption('log');
            $options = [
                'dateFormat' => 'Y-m-d G:i:s',
                'filename' => 'app.log'
            ];
            $logger = new Logger($logCfg['path'], LogLevel::DEBUG, $options);
            self::$appLogger = $logger;
        }

        return self::$appLogger;
    }
}