<?php

namespace App;

use App\models\context\AbstractProxyCheckContext;
use App\models\event\AbstractProxyCheckEvent;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\TransferStats;

class RequestFactory
{
    /**
     * @param Client $client
     * @param AbstractProxyCheckEvent $event
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public static function createRequestForEvent(Client $client, AbstractProxyCheckEvent $event)
    {
        $promise = self::buildGetAsync($client, $event);
        $promise->then(
            function () use ($event) {
                self::assignSuccess($event);
                $event->afterEvent();
            },
            function (RequestException $e) use ($event) {
                self::assignFail($e, $event);
                $event->afterEvent();
            }
        );

        return $promise;
    }

    /**
     * @param Client $client
     * @param AbstractProxyCheckContext $context
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public static function createRequestForContext(Client $client, AbstractProxyCheckContext $context)
    {
        $event = $context->getEvent();
        $promise = self::buildGetAsync($client, $event);
        $promise->then(
            function () use ($context, $event) {
                self::assignSuccess($event);
                $context->handle();
            },
            function (RequestException $e) use ($context, $event) {
                self::assignFail($e, $event);
                $context->handle();
            }
        );

        return $promise;
    }

    /**
     * @param Client $client
     * @param AbstractProxyCheckEvent $event
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    private static function buildGetAsync(Client $client, AbstractProxyCheckEvent $event)
    {
        $proxy = $event->getProxy();
        return $client->getAsync($event->getRequestUrl()->getFullUrl(), [
            'proxy' => $proxy->getFullUrl(),
            'timeout' => $proxy->getTimeout(),
            'on_stats' => function (TransferStats $stats) use ($event) {
                $event->setDuration($stats->getHandlerStats()['total_time']);
            }
        ]);
    }

    private static function assignSuccess(AbstractProxyCheckEvent $event)
    {
        $event->setResult(
            ResultFactory::createSuccess()
        );
    }

    private static function assignFail(RequestException $exception, AbstractProxyCheckEvent $event)
    {
        $event->setResult(
            ResultFactory::createFail($exception)
        );
    }
}