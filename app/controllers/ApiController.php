<?php

namespace App\Controllers;

use App\models\adapter\RowToProxyEntityAdapter;
use App\models\proxy\ProxyEntity;
use App\Registry;
use MiladRahimi\PHPRouter\Request;

class ApiController
{
    function listMethod(Request $request) {
        $resp = [];
        $count = (int) $request->get('count');
        $maxTimeout = (int) $request->get('maxTimeout');
        $maxTimeout = $maxTimeout <= ProxyEntity::DEFAULT_TIMEOUT ?
            ProxyEntity::DEFAULT_TIMEOUT : $maxTimeout
        ;

        if (is_null($count) || (int) $count <= 0) {
            return \GuzzleHttp\json_encode($resp);
        }

        $time = time();

        $proxies = ProxyEntity::getByOrm()
            ->where(['quality' => ProxyEntity::GOOD])
            ->where("timeout <= $maxTimeout")
            ->orderBy('timeout')
            ->orderBy('lastRequestTime')
            ->orderBy('lastCheckTime', 'DESC')
            ->limit($count)
            ->fetchAll();

        $entities = (new RowToProxyEntityAdapter())->getMany($proxies);

        foreach ($entities as $entity) {
            $arr = [];
            $arr['host'] = $entity->getHost();
            $arr['port'] = $entity->getPort();
            $arr['protocol'] = $entity->getProtocol();
            $arr['timeout'] = $entity->getTimeout();
            $entity->setLastRequestTime($time);
            $entity->save();
            $resp[] = $arr;
        }

        return \GuzzleHttp\json_encode($resp);
    }

    function badMethod()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if (isset($data['host']) && isset($data['port'])) {
            $proxy = Registry::getORM()->
                table(ProxyEntity::tableName())
                ->where(['host' => $data['host'], 'port' => $data['port']])
                ->fetch()
            ;

            if ($proxy != null) {
                $proxy = (new RowToProxyEntityAdapter())->getOne($proxy);
                $proxy->setTimeout(ProxyEntity::DEFAULT_TIMEOUT)
                    ->setCheckPriority(ProxyEntity::CHECK_PRIORITY_HIGH)
                    ->setQuality(ProxyEntity::BAD)
                    ->save()
                ;
            }
        }
    }
}