<?php

namespace App\models\handler;

use App\models\event\AbstractProxyCheckEvent;
use App\models\proxy\ProxyEntity;

class ProxyCheckEventResultHandler extends AbstractProxyCheckEventResultHandler
{
    /**
     * @param AbstractProxyCheckEvent $event
     */
    protected function handleSuccess(AbstractProxyCheckEvent $event)
    {
        $proxy = $event->getProxy();
        $proxy->setQuality(ProxyEntity::GOOD);
        $proxy->setTimeout($event->getDuration());
    }

    /**
     * @param AbstractProxyCheckEvent $event
     */
    protected function handleFail(AbstractProxyCheckEvent $event)
    {
        $errorHandler = $this->getErrorHandler();
        $errorHandler->setException(
            $event->getResult()
                ->getException()
        );
        $result = $errorHandler->handle();
        $proxy = $event->getProxy();
        $duration = $event->getDuration();
        switch ($result) {
            case GuzzleRequestErrorHandler::DEAD:
                $proxy->setQuality(ProxyEntity::DEAD);
                $proxy->setTimeout($duration);
                break;
            case GuzzleRequestErrorHandler::REPEAT:
            case GuzzleRequestErrorHandler::BAD:
                if ($proxy->getQuality() == ProxyEntity::REPEAT) {
                    if ($proxy->getTimeout() >= ProxyEntity::MAX_TIMEOUT) {
                        $proxy->setQuality(ProxyEntity::DEAD);
                    } else {
                        $proxy->setQuality(ProxyEntity::REPEAT);
                        $proxy->setTimeout($proxy->getTimeout() + ProxyEntity::INTERVAL);
                    }
                } else {
                    if ($proxy->getTimeout() <= ProxyEntity::MAX_TIMEOUT) {
                        $proxy->setQuality(ProxyEntity::REPEAT);
                        $proxy->setTimeout($proxy->getTimeout() + ProxyEntity::INTERVAL);
                    } else {
                        $proxy->setQuality(ProxyEntity::BAD);
                        $proxy->setTimeout(ProxyEntity::DEFAULT_TIMEOUT);
                    }
                }
                break;
        }
    }
}