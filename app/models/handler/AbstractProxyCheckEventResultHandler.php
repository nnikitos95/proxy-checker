<?php

namespace App\models\handler;

use App\models\event\AbstractProxyCheckEvent;

abstract class AbstractProxyCheckEventResultHandler
{
    /**
     * @var AbstractErrorHandler
     */
    private $errorHandler;

    /**
     * AbstractCheckEventResultHandler constructor.
     * @param AbstractErrorHandler $handler
     */
    public function __construct(AbstractErrorHandler $handler = null)
    {
        $this->errorHandler = $handler;
    }

    public function handle(AbstractProxyCheckEvent $event)
    {
        $result = $event->getResult();
        if ($result->isSuccess()) {
            $this->handleSuccess($event);
        } else {
            $this->handleFail($event);
        }
    }

    /**
     * @return AbstractErrorHandler
     * @throws \Exception
     */
    public function getErrorHandler(): AbstractErrorHandler
    {
        if ($this->errorHandler == null) {
            throw new \Exception('Error handler is null');
        }
        return $this->errorHandler;
    }

    /**
     * @param AbstractProxyCheckEvent $event
     */
    abstract protected function handleSuccess(AbstractProxyCheckEvent $event);

    /**
     * @param AbstractProxyCheckEvent $event
     */
    abstract protected function handleFail(AbstractProxyCheckEvent $event);
}