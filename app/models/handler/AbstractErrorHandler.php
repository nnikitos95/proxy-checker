<?php

namespace App\models\handler;

abstract class AbstractErrorHandler
{
    /**
     * @var \Exception
     */
    protected $exception;

    /**
     * @return mixed
     */
    abstract public function handle();

    /**
     * @param \Exception $exception
     */
    public function setException(\Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->exception->getCode();
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->exception->getMessage();
    }
}