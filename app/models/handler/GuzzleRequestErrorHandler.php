<?php

namespace App\models\handler;

use GuzzleHttp\Exception\RequestException;
use Psr\Log\InvalidArgumentException;

class GuzzleRequestErrorHandler extends AbstractErrorHandler
{
    const DEAD   = 1;
    const REPEAT = 2;
    const BAD    = 3;

    /**
     * @var RequestException
     */
    protected $exception;

    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * Dead error, it means, that proxy is invalid
     * @var array
     */
    private $deadErrors = [
        7,
        403,
        407,
        401,
        52,
        35,
        444,
        406
    ];

    /**
     * Repeat errors, usually timeout is up
     * @var array
     */
    private $repeatErrors = [
        28,
        56,
        429
    ];

    /**
     * @param \Exception $exception
     * @return GuzzleRequestErrorHandler
     */
    public function setException(\Exception $exception): GuzzleRequestErrorHandler
    {
        if (!($exception instanceof RequestException)) {
            throw new InvalidArgumentException('Param $exception must be instance of ' . RequestException::class);
        }
        $this->exception = $exception;
        $this->generateCodeAndMessage();
        return $this;
    }

    public function handle()
    {
        if (in_array($this->code, $this->deadErrors)) {
            return self::DEAD;
        }

        if (in_array($this->code, $this->repeatErrors)) {
            return self::REPEAT;
        }

        return self::BAD;
    }

    private function generateCodeAndMessage()
    {
        $exception = $this->exception;
        $handler = $exception->getHandlerContext();
        $code = $handler['errno'] ?? null;
        $message = $handler['error'] ?? null;
        if ($code == null) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
        }

        $this->code = (int) $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}