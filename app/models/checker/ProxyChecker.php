<?php

namespace App\models\checker;

use App\models\context\AbstractProxyCheckContext;
use App\models\storage\UrlStorage;
use App\Registry;
use App\RequestFactory;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;

class ProxyChecker
{
    /**
     * Max count connections
     * @var string
     */
    const MAX_CONCURRENCY = 25;

    /**
     * @var AbstractProxyCheckContext[]
     */
    private $contexts  = [];

    /**
     * @var UrlStorage
     */
    private $urlStorage;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var int
     */
    private $concurrency = 5;

    /**
     * Checker constructor.
     * @param AbstractProxyCheckContext[] $contexts
     * @param UrlStorage $storage
     * @param Client $client
     */
    public function __construct($contexts, UrlStorage $storage, Client $client = null)
    {
        $this->contexts = $contexts;
        $this->urlStorage = $storage;
        if ($client == null) {
            $client = new Client(['verify' => false]);
        }
        $this->client = $client;
    }

    public function check()
    {
        $requests = $this->makeRequests();

        $count = $this->urlStorage->getCount() / self::MAX_CONCURRENCY > 1 ?
            self::MAX_CONCURRENCY : $this->urlStorage->getCount()
        ;

        $this->setConcurrency($count);

        Registry::getConsoleLogger()->info('Concurrency: ' . $count);

        $pool = new Pool($this->client, $requests(), [
            'concurrency' => $count,
        ]);

        $promise = $pool->promise();

        $promise->wait();
    }

    protected function makeRequests()
    {
        $requests = function () {
            /**
             * @var AbstractProxyCheckContext $context
             */
            foreach ($this->contexts as $context) {
                yield function () use ($context) {
                    $event = $context->getEvent();
                    $url = $this->urlStorage
                        ->getNextUrl(
                            $event->getProxy()
                                ->getProtocol()
                        )
                    ;
                    $event->setRequestUrl($url);
                    return RequestFactory::createRequestForContext($this->client, $context);
                };
            }
        };

        return $requests;
    }

    /**
     * @param int $concurrency
     * @return ProxyChecker
     */
    public function setConcurrency(int $concurrency): ProxyChecker
    {
        if ($concurrency > 0) {
            $this->concurrency = $concurrency;
        }
        return $this;
    }
}