<?php

namespace App\models\context;

use App\models\CallbackTrait;
use App\models\event\AbstractProxyCheckEvent;
use App\models\handler\AbstractProxyCheckEventResultHandler;

abstract class AbstractProxyCheckContext
{
    use CallbackTrait;

    /**
     * @var AbstractProxyCheckEvent
     */
    protected $event;

    /**
     * @var AbstractProxyCheckEventResultHandler
     */
    protected $resultHandler;

    /**
     * AbstractEventContext constructor.
     * @param AbstractProxyCheckEventResultHandler $handler
     * @param AbstractProxyCheckEvent $event
     */
    public function __construct(AbstractProxyCheckEvent $event, AbstractProxyCheckEventResultHandler $handler)
    {
        $this->resultHandler = $handler;
        $this->event = $event;
    }

    final public function handle()
    {
        $this->beforeRunCallBack();
        $this->runCallbacks();
    }

    /**
     * @return AbstractProxyCheckEvent
     */
    public function getEvent(): AbstractProxyCheckEvent
    {
        return $this->event;
    }

    abstract protected function beforeRunCallBack();
}