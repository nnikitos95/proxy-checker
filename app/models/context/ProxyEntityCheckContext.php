<?php

namespace App\models\context;

use App\models\event\ProxyEntityCheckEvent;
use App\models\handler\AbstractProxyCheckEventResultHandler;
use App\models\proxy\ProxyEntity;

class ProxyEntityCheckContext extends AbstractProxyCheckContext
{
    public function __construct(ProxyEntityCheckEvent $event, AbstractProxyCheckEventResultHandler $handler)
    {
        parent::__construct($event, $handler);
    }

    /**
     * @inheritdoc
     */
    protected function beforeRunCallBack()
    {
        $this->resultHandler->handle($this->event);
        /**
         * @var ProxyEntity $proxy
         */
        $proxy = $this->event->getProxy();
        $proxy->setLastCheckTime(time());
        $proxy->save();
        $this->event->afterEvent();
    }
}