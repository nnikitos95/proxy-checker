<?php

namespace App\models\proxy;

use App\models\url\AbstractUrl;

abstract class AbstractProxy extends AbstractUrl implements ProxyStateInterface
{}