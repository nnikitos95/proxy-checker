<?php

namespace App\models\proxy;

use App\Registry;
use LessQL\Row;

/**
 * Class ProxyEntity
 * @package App
 */
class ProxyEntity extends AbstractProxy
{
    /**
     * Quality of the proxy
     * @var string
     */
    const GOOD   = 'GOOD';
    /**
     * Quality of the proxy
     * @var string
     */
    const  BAD   = 'BAD';
    /**
     * Quality of the proxy
     * @var string
     */
    const DEAD   = 'DEAD';
    /**
     * Quality of the proxy
     * @var string
     */
    const REPEAT = 'REPEAT';
    /**
     * Proxy protocol
     * @var string
     */
    const PROTOCOL_HTTP  = 'HTTP';
    /**
     * Proxy protocol
     * @var string
     */
    const PROTOCOL_HTTPS = 'HTTPS';
    /**
     * @var string
     */
    const DEFAULT_TIMEOUT = 20;
    const MAX_TIMEOUT     = 60;
    const INTERVAL        = 5;

    const CHECK_PRIORITY_HIGH = 1;
    const CHECK_PRIORITY_LOW  = 0;

    /**
     * @var Row
     */
    private $row;

    /**
     * ProxyEntity constructor.
     * @param Row $row
     */
    public function __construct(Row $row)
    {
        $this->row = $row;
    }

    /**
     * @param array $params
     * @return ProxyEntity
     */
    public static function create(array $params = []): ProxyEntity
    {
        $row = self::getByOrm()->createRow($params);
        return new ProxyEntity($row);
    }

    /**
     * @param Row $row
     * @return $this
     */
    public function setRow(Row $row): ProxyEntity
    {
        $this->row = $row;
        return $this;
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'proxy_entity';
    }

    /**
     * @return int|[]
     */
    public function getId()
    {
        return $this->row->exists() ? $this->row->id : null;
    }

    /**
     * @param string $host
     * @return ProxyEntity
     */
    public function setHost(string $host): ProxyEntity
    {
        $this->row->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->row->host;
    }

    /**
     * @param string $port
     * @return ProxyEntity
     */
    public function setPort(string $port): ProxyEntity
    {
        $this->row->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->row->port;
    }

    /**
     * @param int $timeout
     * @return ProxyEntity
     */
    public function setTimeout(int $timeout): ProxyEntity
    {
        if ($timeout < self::DEFAULT_TIMEOUT) {
            $timeout = self::DEFAULT_TIMEOUT;
        }
        $this->row->timeout = $timeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->row->timeout;
    }

    /**
     * @param int $lastRequestTime
     * @return ProxyEntity
     */
    public function setLastRequestTime(int $lastRequestTime): ProxyEntity
    {
        $this->row->lastRequestTime = $lastRequestTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getLastRequestTime(): int
    {
        return $this->row->lastRequestTime;
    }

    /**
     * @param int $lastCheckTime
     * @return ProxyEntity
     */
    public function setLastCheckTime(int $lastCheckTime): ProxyEntity
    {
        $this->row->lastCheckTime = $lastCheckTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getLastCheckTime(): int
    {
        return $this->row->lastCheckTime;
    }

    /**
     * @param string $quality
     * @return ProxyEntity
     */
    public function setQuality(string $quality)
    {
        if (!in_array($quality, [self::REPEAT, self::BAD, self::GOOD, self::DEAD])) {
            $quality = self::BAD;
        }

        $this->row->quality = $quality;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuality(): string
    {
        return $this->row->quality;
    }

    /**
     * @param string $protocol
     * @return ProxyEntity
     */
    public function setProtocol(string $protocol): ProxyEntity
    {
        if (!in_array(strtoupper($protocol), [self::PROTOCOL_HTTP, self::PROTOCOL_HTTPS])) {
            $protocol = self::PROTOCOL_HTTP;
        }

        $this->row->protocol = $protocol;

        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->row->protocol;
    }

    /**
     * @param int $priority
     * @return ProxyEntity
     */
    public function setCheckPriority(int $priority): ProxyEntity
    {
        if (!in_array($priority, [self::CHECK_PRIORITY_HIGH, self::CHECK_PRIORITY_LOW])) {
            $priority = self::CHECK_PRIORITY_LOW;
        }

        $this->row->checkPriority = $priority;
        return $this;
    }

    /**
     * @return int
     */
    public function getCheckPriority()
    {
        return $this->row->priority;
    }

    /**
     * @return ProxyEntity
     */
    public function save(): ProxyEntity
    {
        $this->row->save();
        return $this;
    }

    /**
     * @param $data
     * @return ProxyEntity
     */
    public function update($data): ProxyEntity
    {
        $this->row->update($data);
        return $this;
    }

    /**
     * @return \LessQL\Result|Row|null
     */
    public static function getByOrm()
    {
        return Registry::getORM()->table(self::tableName());
    }
}