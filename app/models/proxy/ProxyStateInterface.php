<?php

namespace App\models\proxy;

interface ProxyStateInterface
{
    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout);

    /**
     * @return int
     */
    public function getTimeout();

    /**
     * @return string
     */
    public function getQuality(): string;

    /**
     * @param string $quality
     */
    public function setQuality(string $quality);
}