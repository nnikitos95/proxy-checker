<?php

namespace App\models\proxy;

use App\models\storage\ProxyStorageTrait;
use App\models\storage\UrlStorageTrait;

class Proxy extends AbstractProxy
{
    use UrlStorageTrait, ProxyStorageTrait {
        UrlStorageTrait::__construct as protected UrlConstruct;
        ProxyStorageTrait::__construct as protected ProxyConstruct;
    }

    /**
     * Proxy constructor.
     * @param AbstractProxy|string $proxy
     */
    public function __construct($proxy)
    {
        if ($proxy instanceof AbstractProxy) {
            $this->ProxyConstruct($proxy);
            $this->UrlConstruct($proxy->getFullUrl());
        } else {
            $this->UrlConstruct($proxy);
        }
    }

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol(string $protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * @return string
     */
    public function getQuality(): string
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     */
    public function setQuality(string $quality)
    {
        $this->quality = $quality;
    }
}