<?php

namespace App\models\proxy;

use App\models\storage\ProxyStorageTrait;

class ProxyState implements ProxyStateInterface
{
    use ProxyStorageTrait;

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @return string
     */
    public function getQuality(): string
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     */
    public function setQuality(string $quality)
    {
        $this->quality = $quality;
    }
}