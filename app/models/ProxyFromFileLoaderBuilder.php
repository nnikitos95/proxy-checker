<?php

namespace App\models;

use App\models\adapter\FileDataToLoadDataAdapter;
use App\models\provider\ProxyFromFileDataProvider;
use App\ProxyLoader;

class ProxyFromFileLoaderBuilder implements ProxyLoaderBuilderInterface
{
    /**
     * @var string
     */
    protected $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return ProxyLoader
     */
    public function build()
    {
        $provider = new ProxyFromFileDataProvider($this->filePath);
        $adapter  = new FileDataToLoadDataAdapter();
        $loader = new ProxyLoader($provider, $adapter);
        return $loader;
    }
}