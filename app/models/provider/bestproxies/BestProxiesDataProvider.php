<?php

namespace App\models\provider\bestproxies;

use App\models\provider\AbstractProxyDataFromRequestProvider;
use App\ProxyListParser;
use Psr\Http\Message\ResponseInterface;

class BestProxiesDataProvider extends AbstractProxyDataFromRequestProvider
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param ResponseInterface $r
     * @param string $url
     */
    protected function interpretResponse(ResponseInterface $r, string $url)
    {
        $parser = new ProxyListParser($r->getBody());
        $this->data = $parser->parse();
        $this->count = count($this->data);
    }
}