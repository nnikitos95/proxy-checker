<?php

namespace App\models\provider;

use App\models\proxy\AbstractProxy;
use App\models\proxy\Proxy;
use App\models\url\UrlGeneratorInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractProxyDataFromRequestProvider extends AbstractProxyDataProvider
{
    /**
     * @var Proxy
     */
    protected $proxyUrl;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * AbstractProxyDataProvider constructor.
     * @param UrlGeneratorInterface $generator
     * @param AbstractProxy|null $proxyUrl
     */
    public function __construct(UrlGeneratorInterface $generator, AbstractProxy $proxyUrl = null)
    {
        $this->urlGenerator = $generator;
        $this->proxyUrl = $proxyUrl;
    }

    public function downloadData()
    {
        $options = [];
        if ($this->proxyUrl != null) {
            $options = [
                'proxy' => $this->proxyUrl->getFullUrl(),
                'timeout' => $this->proxyUrl->getTimeout()
            ];
        }
        $client = new Client($options);
        $requests = function () use ($client) {
            foreach ($this->urlGenerator->getUrlList() as $url) {
                yield function () use ($client, $url) {
                    return $client->getAsync($url)
                        ->then(
                            function (ResponseInterface $r) use ($url) {
                                $this->interpretResponse($r, $url);
                            },
                            function () {
                            }
                        );
                };
            }
        };

        $pool = new Pool($client, $requests(), [
            'concurrency' => 5,
        ]);

        $pool->promise()
            ->wait()
        ;
    }

    /**
     * @param ResponseInterface $r
     * @param string $url
     */
    abstract protected function interpretResponse(ResponseInterface $r, string $url);
}