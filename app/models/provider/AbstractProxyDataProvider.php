<?php

namespace App\models\provider;

abstract class AbstractProxyDataProvider
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @return void
     */
    abstract public function downloadData();

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function hasData(): bool
    {
        return $this->count > 0;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}