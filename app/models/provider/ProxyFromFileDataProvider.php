<?php

namespace App\models\provider;

use App\ProxyListParser;

class ProxyFromFileDataProvider extends AbstractProxyDataProvider
{
    /**
     * @var string
     */
    protected $fileStr;

    /**
     * ProxyFromFileDataProvider constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        $this->fileStr = file_get_contents($filePath);
    }

    /**
     * @return void
     */
    public function downloadData()
    {
        $parser = new ProxyListParser($this->fileStr);
        $this->data = $parser->parse();
        $this->count = count($this->data);
    }
}