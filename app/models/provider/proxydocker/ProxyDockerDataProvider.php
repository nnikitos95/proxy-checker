<?php

namespace App\models\provider\proxydocker;

use App\models\provider\AbstractProxyDataFromRequestProvider;
use App\Registry;
use Psr\Http\Message\ResponseInterface;

class ProxyDockerDataProvider extends AbstractProxyDataFromRequestProvider
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param ResponseInterface $r
     * @param string $url
     */
    protected function interpretResponse(ResponseInterface $r, string $url)
    {
        $body = $r->getBody();
        $decoded = json_decode($body, true);
        $count = count($decoded['Proxies']);
        if ($count == 0) {
            Registry::getConsoleLogger()
                ->info("Empty $url");
        }
        if ($count == 5000) {
            Registry::getConsoleLogger()
                ->info("MaxRows $url");
        }
        if ($count > 0) {
            array_push($this->data, ...$decoded['Proxies']);
        }
        $this->count += $count;
    }
}