<?php

namespace App\models\url;

abstract class AbstractUrl
{
    /**
     * @param string $host
     */
    abstract public function setHost(string $host);

    /**
     * @return string
     */
    abstract public function getHost(): string;

    /**
     * @param string $port
     */
    abstract public function setPort(string $port);

    /**
     * @return string
     */
    abstract public function getPort(): string;

    /**
     * @param string $protocol
     */
    abstract public function setProtocol(string $protocol);

    /**
     * @return string
     */
    abstract public function getProtocol(): string;

    /**
     * Return host with port, if it exists
     * @return string
     */
    public function getAddress(): string
    {
        $port = empty($this->getPort()) ? '' : ':' . $this->getPort();
        return $this->getHost() . $port;
    }

    /**
     * Return full url with protocol
     * @return string
     */
    public function getFullUrl(): string
    {
        $protocol = empty($this->getProtocol()) ? '' : $this->getProtocol() . '://';
        return strtolower($protocol) . $this->getAddress();
    }

    public function fromString(string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \InvalidArgumentException('Param $url is nol url');
        }
        $url = parse_url($url);
        $host = $url['host'] ?? '';
        $host .= $url['path'] ?? '';
        $host .= isset($url['query']) ? '?' . $url['query'] : '';

        $this->setHost($host);
        $this->setProtocol($url['scheme']);

        if (isset($url['port'])) {
            $this->setPort($url['port']);
        }
    }
}