<?php

namespace App\models\url;

class BestProxiesUrlGenerator implements UrlGeneratorInterface
{
    protected $config = [];

    /**
     * BestProxiesUrlGenerator constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getUrlList()
    {
        $host = $this->config['connection']['host'];
        $params = $this->config['params'];

        $url = $host . '?';
        foreach ($params as $key => $value) {
            $url .= $key;
            if (gettype($value) == 'array') {
                $url .= '=' . implode(",", $value);
            } else {
                if (strlen($value) != 0) {
                    $url .=  '=' . $value;
                }
            }
            $url .= '&';
        }
        $url = mb_strimwidth($url, 0, strlen($url) - 1);
        return [$url];
    }
}