<?php

namespace App\models\url;

use function BenTools\CartesianProduct\cartesian_product;
use function GuzzleHttp\Psr7\build_query;

class ProxyDockerUrlGenerator implements UrlGeneratorInterface
{
    const ANONYMITY_ELITE     = 'ELITE';
    const ANONYMITY_ANONYMOUS = 'ANONYMOUS';

    const PROTOCOL_HTTP = 'HTTP';
    const PROTOCOL_HTTPS = 'HTTPS';

    private $countries = [];
    private $anonymities = [];
    private $types = [];
    private $needs = [];
    private $defaultPairs = [
        'city'   => 'All',
        'port'   => 'All',
        'state'  => 'All',
//        'need'   => 'All',
        'format' => 'json',
//        'country' => 'All'
    ];

    private $host;
    private $email;

    public function __construct(string $host, string $email)
    {
        $this->host = $host;
        $this->email = $email;
    }

    /**
     * Return assoc array like
     *
     * `[
     *      'HTTP' => [.....],
     *      'HTTPS => [.....],
     *      'etc'
     * ]`
     * @return array
     */
    public function getUrlList(): array
    {
        $arr = [
            'country' => $this->countries,
            'type' => $this->types,
            'anonymity' => $this->anonymities,
            'need' => $this->needs
        ];

        $newArr = [];
        foreach (cartesian_product($arr) as $row) {
            $pairs = $this->defaultPairs;
            $pairs = array_merge($pairs, [
                'email' => $this->email,
                'need' => $row['need'],
                'anonymity' => $row['anonymity'],
                'type' => $row['type'],
                'country' => $row['country']
            ]);

            $newArr[] = $this->host . '?' . build_query($pairs);
        }

        return $newArr;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function addCountry(string $country): ProxyDockerUrlGenerator
    {
        if (!in_array($country, $this->countries)) {
            $this->countries[] = $country;
        }
        return $this;
    }

    /**
     * @param string $anonymity
     * @return ProxyDockerUrlGenerator
     */
    public function addAnonymity(string $anonymity): ProxyDockerUrlGenerator
    {
        if (in_array($anonymity, [self::ANONYMITY_ANONYMOUS, self::ANONYMITY_ELITE])) {
            $this->anonymities[] = $anonymity;
        }
        return $this;
    }

    /**
     * @param string $type
     * @return ProxyDockerUrlGenerator
     */
    public function addType(string $type): ProxyDockerUrlGenerator
    {
        if (in_array($type, [self::PROTOCOL_HTTPS, self::PROTOCOL_HTTP])) {
            $this->types[] = $type;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $countries
     * @return ProxyDockerUrlGenerator
     */
    public function setCountries(array $countries): ProxyDockerUrlGenerator
    {
        if (!empty($countries)) {
            foreach ($countries as $country) {
                $this->addCountry($country);
            }
        }
        return $this;
    }

    /**
     * @param array $anonymities
     * @return ProxyDockerUrlGenerator
     */
    public function setAnonymities(array $anonymities): ProxyDockerUrlGenerator
    {
        if (!empty($anonymities)) {
            foreach ($anonymities as $anonymity) {
                $this->addAnonymity($anonymity);
            }
        }
        return $this;
    }

    /**
     * @param array $types
     * @return ProxyDockerUrlGenerator
     */
    public function setTypes(array $types): ProxyDockerUrlGenerator
    {
        if (!empty($types)) {
            foreach ($types as $type) {
                $this->addType($type);
            }
        }
        return $this;
    }

    /**
     * @param array $needs
     * @return ProxyDockerUrlGenerator
     */
    public function setNeeds(array $needs): ProxyDockerUrlGenerator
    {
        $this->needs = $needs;
        return $this;
    }
}