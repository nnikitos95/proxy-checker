<?php

namespace App\models\url;

interface UrlGeneratorInterface
{
    /**
     * @return array
     */
    public function getUrlList();
}