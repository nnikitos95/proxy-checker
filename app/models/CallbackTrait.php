<?php

namespace App\models;

trait CallbackTrait
{
    /**
     * @var \Closure[]
     */
    protected $callbackArray = [];

    /**
     * @param \Closure $callback
     */
    public function addCallback(\Closure $callback)
    {
        $this->callbackArray[] = $callback;
    }

    private function runCallbacks()
    {
        foreach ($this->callbackArray as $closure) {
            $closure($this);
        }
    }
}