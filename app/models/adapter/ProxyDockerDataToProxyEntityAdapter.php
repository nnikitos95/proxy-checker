<?php

namespace App\models\adapter;

use App\models\proxy\ProxyEntity;

class ProxyDockerDataToProxyEntityAdapter implements ProxyEntityAdapterInterface
{
    /**
     * @param array $data
     * @return ProxyEntity[]
     */
    public function getMany($data)
    {
        $arr = [];
        foreach ($data as $row) {
            $arr[] = $this->getOne($row);
        }

        return $arr;
    }

    /**
     * @param array $data
     * @return ProxyEntity
     */
    public function getOne($data): ProxyEntity
    {
        $p = ProxyEntity::create();
        $p->setHost($data['ip'])
            ->setPort($data['port'])
            ->setProtocol($data['type'])
        ;

        return $p;
    }
}