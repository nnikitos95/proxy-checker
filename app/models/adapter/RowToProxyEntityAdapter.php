<?php

namespace App\models\adapter;

use App\models\proxy\ProxyEntity;
use LessQL\Row;

class RowToProxyEntityAdapter implements ProxyEntityAdapterInterface
{
    /**
     * @param Row $row
     * @return ProxyEntity
     */
    public function getOne($row): ProxyEntity
    {
        return new ProxyEntity($row);
    }

    /**
     * @param Row[] $rows
     * @return ProxyEntity[]
     */
    public function getMany($rows)
    {
        $arr = [];
        foreach ($rows as $row) {
            $arr[] = $this->getOne($row);
        }

        return $arr;
    }
}