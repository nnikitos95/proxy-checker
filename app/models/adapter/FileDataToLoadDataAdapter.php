<?php

namespace App\models\adapter;

use App\models\provider\AbstractProxyDataProvider;

class FileDataToLoadDataAdapter extends AbstractProxyDataToLoadDataAdapter
{
    /**
     * @param AbstractProxyDataProvider $provider
     * @return array
     */
    public function getData(AbstractProxyDataProvider $provider): array
    {
        $newData = [];
        foreach ($provider->getData() as $row) {
            $newData[] = $this->getRow($row['host'], $row['port'], strtoupper($row['type']));
        }
        return $newData;
    }
}