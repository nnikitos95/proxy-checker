<?php

namespace App\models\adapter;

use App\models\context\ProxyEntityCheckContext;
use App\models\event\ProxyEntityCheckEvent;
use App\models\handler\ProxyCheckEventResultHandler;
use App\models\handler\GuzzleRequestErrorHandler;
use App\models\proxy\ProxyEntity;

class ProxyEntityToProxyEntityCheckContextAdapter
{
    /**
     * @param ProxyEntity[] $array
     * @param \Closure[] $callbacks
     * @return ProxyEntityCheckContext[]
     */
    public function getMany($array, array $callbacks = [])
    {
        $arr = [];
        foreach ($array as $proxy) {
            $arr[] = $this->getOne($proxy, $callbacks);
        }
        return $arr;
    }

    /**
     * @param ProxyEntity $entity
     * @param \Closure[] $callbacks
     * @return ProxyEntityCheckContext
     */
    public function getOne(ProxyEntity $entity, array $callbacks = [])
    {
        $resultHandler = new ProxyCheckEventResultHandler(new GuzzleRequestErrorHandler());
        $event = new ProxyEntityCheckEvent($entity);
        if (!empty($callbacks)) {
            foreach ($callbacks as $callback) {
                $event->addCallback($callback);
            }
        }
        return new ProxyEntityCheckContext($event, $resultHandler);
    }
}