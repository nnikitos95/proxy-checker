<?php

namespace App\models\adapter;

use App\models\provider\AbstractProxyDataProvider;

abstract class AbstractProxyDataToLoadDataAdapter
{
    /**
     * @param AbstractProxyDataProvider $provider
     * @return array
     */
    abstract public function getData(AbstractProxyDataProvider $provider): array;

    /**
     * @param string $host
     * @param string $port
     * @param string $protocol
     * @return array
     */
    protected function getRow(string $host, string $port, string $protocol): array
    {
        return [
            'host' => $host,
            'port' => $port,
            'protocol' => $protocol
        ];
    }
}