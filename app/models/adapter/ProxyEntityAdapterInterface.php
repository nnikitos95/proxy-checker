<?php

namespace App\models\adapter;

use App\models\proxy\ProxyEntity;

interface ProxyEntityAdapterInterface
{
    /**
     * @param mixed $data
     * @return ProxyEntity
     */
    public function getOne($data): ProxyEntity;

    /**
     * @param mixed $data
     * @return ProxyEntity[]
     */
    public function getMany($data);
}