<?php

namespace App\models\adapter;

use App\models\provider\AbstractProxyDataProvider;

class ProxyDockerDataToLoadDataAdapter extends AbstractProxyDataToLoadDataAdapter
{
    /**
     * @param AbstractProxyDataProvider $provider
     * @return array
     */
    public function getData(AbstractProxyDataProvider $provider): array
    {
        $data = $provider->getData();
        $array = [];
        foreach ($data as $row) {
            $array[] = $this->getRow($row['ip'], $row['port'], $row['type']);
        }

        return $array;
    }
}