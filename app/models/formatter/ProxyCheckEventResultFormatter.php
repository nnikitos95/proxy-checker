<?php

namespace App\models\formatter;

use App\models\event\AbstractProxyCheckEvent;

class ProxyCheckEventResultFormatter extends AbstractProxyCheckResultFormatter
{
    /**
     * @param AbstractProxyCheckEvent $event
     * @return array
     */
    protected function getProxyIdentifier(AbstractProxyCheckEvent $event)
    {
        return [
            '/proxy_identifier/' => $event->getProxy()->getFullUrl()
        ];
    }
}