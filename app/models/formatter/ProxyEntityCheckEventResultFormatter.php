<?php

namespace App\models\formatter;

use App\models\event\AbstractProxyCheckEvent;
use App\models\event\ProxyEntityCheckEvent;
use Psr\Log\InvalidArgumentException;

class ProxyEntityCheckEventResultFormatter extends AbstractProxyCheckResultFormatter
{
    /**
     * @param AbstractProxyCheckEvent $event
     * @return array
     */
    protected function getProxyIdentifier(AbstractProxyCheckEvent $event)
    {
        if (!($event instanceof ProxyEntityCheckEvent)) {
            throw new InvalidArgumentException('Param $event must be instance of' . ProxyEntityCheckEvent::class);
        }
        /**
         * @var \App\models\proxy\ProxyEntity $proxy
         */
        $proxy = $event->getProxy();
        return [
            '/proxy_identifier/' => $proxy->getId()
        ];
    }
}