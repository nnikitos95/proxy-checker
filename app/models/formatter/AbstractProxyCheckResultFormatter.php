<?php

namespace App\models\formatter;

use App\models\event\AbstractProxyCheckEvent;

abstract class AbstractProxyCheckResultFormatter
{
    /**
     * @var string
     */
    protected $format = '[result][proxy_identifier][link][durations][time1s->time2s][state1->state2]';

    /**
     * @var string
     */
    protected $errorFormat = '[code, message]';

    /**
     * @param AbstractProxyCheckEvent $event
     * @return string
     */
    public function format(AbstractProxyCheckEvent $event)
    {
        $baseArray = $this->getBaseArray($event);
        $result = $event->getResult();
        $formatStr = $this->format;
        if ($result->isSuccess()) {
            $baseArray['/result/'] = 'SUCCESS';
        } else {
            $baseArray['/result/'] = 'FAIL';
            $formatStr .= $this->errorFormat;
            $baseArray = array_merge($baseArray, $this->getErrorArray($event));
        }
        return $this->getReplaceStr($baseArray, $formatStr);
    }

    /**
     * @param AbstractProxyCheckEvent $event
     * @return array
     */
    private function getBaseArray(AbstractProxyCheckEvent $event)
    {
        return array_merge([
            '/link/'   => $event->getRequestUrl()->getFullUrl(),
            '/duration/'   => $event->getDuration(),
            '/time1/'  => $event->getPreviousState()->getTimeout(),
            '/time2/'  => $event->getProxy()->getTimeout(),
            '/state1/' => $event->getPreviousState()->getQuality(),
            '/state2/' => $event->getProxy()->getQuality(),
        ], $this->getProxyIdentifier($event));
    }

    /**
     * @param AbstractProxyCheckEvent $event
     * @return array
     */
    private function getErrorArray(AbstractProxyCheckEvent $event)
    {
        $e = $event->getResult()->getException();
        return [
            '/code/' => $e->getCode(),
            '/message/' => $e->getMessage()
        ];
    }

    /**
     * @param array $array
     * @param string $formatStr
     * @return string
     */
    private function getReplaceStr(array $array, string $formatStr)
    {
        return preg_replace(array_keys($array), array_values($array), $formatStr);
    }

    /**
     * @param AbstractProxyCheckEvent $event
     * @return array
     */
    abstract protected function getProxyIdentifier(AbstractProxyCheckEvent $event);
}