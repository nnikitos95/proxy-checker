<?php

namespace App\models;

use App\ProxyLoader;

interface ProxyLoaderBuilderInterface
{
    /**
     * @return ProxyLoader
     */
    public function build();
}