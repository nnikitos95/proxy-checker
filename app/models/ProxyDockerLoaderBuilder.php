<?php

namespace App\models;

use App\ProxyLoader;
use App\models\adapter\ProxyDockerDataToLoadDataAdapter;
use App\models\provider\proxydocker\ProxyDockerDataProvider;
use App\models\url\ProxyDockerUrlGenerator;

class ProxyDockerLoaderBuilder implements ProxyLoaderBuilderInterface
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ProxyDockerLoader constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return ProxyLoader
     */
    public function build()
    {
        $generator    = $this->initUrlGenerator();
        $dataProvider = $this->initDataProvider($generator);
        $adapter      = $this->initAdapter();
        return new ProxyLoader($dataProvider, $adapter);
    }

    /**
     * @return ProxyDockerUrlGenerator
     */
    private function initUrlGenerator()
    {
        $connection = $this->config['connection'];
        $generator = new ProxyDockerUrlGenerator($connection['host'], $connection['email']);
        $params = $this->config['params'];
        $generator->setTypes($params['types'])
            ->setAnonymities($params['anonymities'])
            ->setCountries($params['countries'])
            ->setNeeds($params['needs'])
        ;

        return $generator;
    }

    /**
     * @param ProxyDockerUrlGenerator $generator
     * @return ProxyDockerDataProvider
     */
    private function initDataProvider(ProxyDockerUrlGenerator $generator)
    {
        return new ProxyDockerDataProvider($generator);
    }

    /**
     * @return ProxyDockerDataToLoadDataAdapter
     */
    private function initAdapter()
    {
        return new ProxyDockerDataToLoadDataAdapter();
    }
}