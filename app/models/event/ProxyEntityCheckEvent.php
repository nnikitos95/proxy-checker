<?php

namespace App\models\event;

use App\models\proxy\ProxyEntity;
use App\models\url\AbstractUrl;

class ProxyEntityCheckEvent extends AbstractProxyCheckEvent
{
    public function __construct(ProxyEntity $proxy, AbstractUrl $requestUrl = null)
    {
        parent::__construct($proxy, $requestUrl);
    }

    protected function beforeRunCallbacks()
    {
        // TODO: Implement beforeRunCallbacks() method.
    }
}