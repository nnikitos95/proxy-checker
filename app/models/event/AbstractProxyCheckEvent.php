<?php

namespace App\models\event;

use App\models\proxy\AbstractProxy;
use App\models\proxy\ProxyState;
use App\models\proxy\ProxyStateInterface;
use App\models\url\AbstractUrl;

abstract class AbstractProxyCheckEvent extends AbstractEvent
{
    /**
     * @var AbstractProxy
     */
    protected $proxy;

    /**
     * @var AbstractUrl
     */
    protected $requestUrl;

    /**
     * @var ProxyStateInterface
     */
    protected $previousState;

    /**
     * @var integer
     */
    protected $duration;

    /**
     * ProxyRequestEvent constructor.
     * @param AbstractProxy $proxy
     * @param AbstractUrl $requestUrl
     */
    public function __construct(AbstractProxy $proxy, AbstractUrl $requestUrl = null)
    {
        $this->proxy = $proxy;
        $this->requestUrl = $requestUrl;
        $this->previousState = new ProxyState($proxy);
    }

    /**
     * @param int $duration
     * @return AbstractProxyCheckEvent
     */
    public function setDuration(int $duration): AbstractProxyCheckEvent
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param AbstractUrl $requestUrl
     * @return AbstractProxyCheckEvent
     */
    public function setRequestUrl(AbstractUrl $requestUrl): AbstractProxyCheckEvent
    {
        $this->requestUrl = $requestUrl;
        return $this;
    }

    /**
     * @return AbstractUrl
     */
    public function getRequestUrl(): AbstractUrl
    {
        return $this->requestUrl;
    }

    /**
     * @return AbstractProxy
     */
    public function getProxy(): AbstractProxy
    {
        return $this->proxy;
    }

    /**
     * @return ProxyStateInterface
     */
    public function getPreviousState(): ProxyStateInterface
    {
        return $this->previousState;
    }
}