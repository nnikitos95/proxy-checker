<?php

namespace App\models\event;

use App\models\CallbackTrait;
use App\Result;

abstract class AbstractEvent
{
    use CallbackTrait;

    /**
     * @var bool
     */
    protected $isHappened = false;

    /**
     * @var Result
     */
    protected $result;

    /**
     * @param Result $result
     * @return AbstractEvent
     */
    public function setResult(Result $result): AbstractEvent
    {
        $this->result = $result;
        $this->isHappened = true;
        return $this;
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        $this->throwExceptionIfNotHappened();
        return $this->result;
    }

    /**
     * @throws \Exception
     */
    protected function throwExceptionIfNotHappened()
    {
        if (!$this->isHappened()) {
            throw new \Exception("Event has not happened yet");
        }
    }

    /**
     * @return bool
     */
    public function isHappened(): bool
    {
        return $this->isHappened;
    }

    final public function afterEvent()
    {
        $this->beforeRunCallbacks();
        $this->runCallbacks();
    }

    abstract protected function beforeRunCallbacks();
}