<?php

namespace App\models\storage;

trait UrlStorageTrait
{
    /**
     * @var @string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $protocol;

    public function __construct(string $url)
    {
        $this->port = '';
        $this->fromString($url);
    }
}