<?php

namespace App\models\storage;

use App\models\url\AbstractUrl;
use App\models\url\Url;

class UrlStorage
{
    /**
     * @var string[]
     */
    protected $urlList = [];

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * UrlStorage constructor.
     * @param string[] $list
     */
    public function __construct($list)
    {
        $this->urlList = $list;
        $this->count = count($list);
    }

    /**
     * @param string $protocol
     * @return AbstractUrl
     */
    public function getNextUrl(string $protocol)
    {
        return new Url($this->getNextUrlAsString($protocol));
    }

    /**
     * @param string $protocol
     * @return string
     */
    public function getNextUrlAsString(string $protocol)
    {
        $url = '';
        while (true) {
            $url = $this->urlList[rand(0, $this->count - 1)];
            $_url = parse_url($url);
            if (strlen($protocol) <= strlen($_url['scheme'])) {
                break;
            }
        }
        return $url;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}