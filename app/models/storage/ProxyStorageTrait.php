<?php

namespace App\models\storage;

use App\models\proxy\ProxyStateInterface;

trait ProxyStorageTrait
{
    /**
     * @var integer
     */
    private $timeout;

    /**
     * @var string
     */
    protected $quality;

    public function __construct(ProxyStateInterface $proxy = null)
    {
        if ($proxy != null) {
            $this->setTimeout($proxy->getTimeout());
            $this->setQuality($proxy->getQuality());
        }
    }
}