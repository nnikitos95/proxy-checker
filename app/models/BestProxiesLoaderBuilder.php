<?php

namespace App\models;

use App\models\adapter\FileDataToLoadDataAdapter;
use App\models\provider\bestproxies\BestProxiesDataProvider;
use App\models\url\BestProxiesUrlGenerator;
use App\ProxyLoader;

class BestProxiesLoaderBuilder implements ProxyLoaderBuilderInterface
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ProxyDockerLoader constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return ProxyLoader
     */
    public function build()
    {
        $generator    = new BestProxiesUrlGenerator($this->config);
        $dataProvider = new BestProxiesDataProvider($generator);
        $adapter      = new FileDataToLoadDataAdapter();
        return new ProxyLoader($dataProvider, $adapter);
    }
}