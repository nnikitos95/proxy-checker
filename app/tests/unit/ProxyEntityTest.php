<?php

namespace App\models\tests\unit;

use App\models\proxy\ProxyEntity;
use PHPUnit\Framework\TestCase;

class ProxyEntityTest extends TestCase
{
    public function testCreateProxyEntity()
    {
        $entity = ProxyEntity::create();
        $this->assertEquals(null, $entity->getId());
        $this->expectException(\TypeError::class);
        $this->assertEquals(null, $entity->getHost());
        $this->expectException(\TypeError::class);
        // etc.
    }

    public function testCreateProxyEntityWithFields()
    {
        $entity = ProxyEntity::create([
            'host' => '192.168.0.1',
            'port' => '8080',
            'protocol' => 'HTTP',
            'quality' => 'BAD'
        ]);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals('192.168.0.1', $entity->getHost());
        $this->assertEquals('8080', $entity->getPort());
        $this->assertEquals('HTTP', $entity->getProtocol());
        $this->assertEquals('BAD', $entity->getQuality());
    }
}