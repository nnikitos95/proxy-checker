<?php

namespace App\models\tests\unit;

use App\models\url\Url;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExpectExceptionWhenCreateUrl()
    {
        new Url('');
    }

    public function testCreateSimpleUrl()
    {
        $url = 'http://www.vl.ru';
        $urlObject = new Url($url);
        $this->assertEquals('www.vl.ru', $urlObject->getHost());
        $this->assertEquals('http', $urlObject->getProtocol());
        $this->assertEquals('', $urlObject->getPort());
        $this->assertEquals('www.vl.ru', $urlObject->getAddress());
        $this->assertEquals($url, $urlObject->getFullUrl());
    }

    public function testCreateSimpleUrlWithPort()
    {
        $url = 'http://www.vl.ru:880';
        $urlObject = new Url($url);
        $this->assertEquals('www.vl.ru', $urlObject->getHost());
        $this->assertEquals('http', $urlObject->getProtocol());
        $this->assertEquals('880', $urlObject->getPort());
        $this->assertEquals('www.vl.ru:880', $urlObject->getAddress());
        $this->assertEquals($url, $urlObject->getFullUrl());
    }
}
