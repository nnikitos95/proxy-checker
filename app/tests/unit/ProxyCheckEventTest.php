<?php

namespace App\models\tests\unit;

use App\models\event\AbstractProxyCheckEvent;
use App\models\formatter\ProxyCheckEventResultFormatter;
use App\models\proxy\AbstractProxy;
use App\models\proxy\Proxy;
use App\models\url\AbstractUrl;
use App\models\url\Url;
use App\RequestFactory;
use App\ResultFactory;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ProxyCheckEventTest extends TestCase
{
    /**
     * @return AbstractProxy
     */
    public function createProxy()
    {
        $proxy = new Proxy('http://103.10.81.172:80');
        $proxy->setTimeout(20);
        $proxy->setQuality('BAD');
        return $proxy;
    }

    /**
     * @return AbstractUrl
     */
    public function createUrl()
    {
        return new Url('http://vl.ru');
    }

    /**
     * @return AbstractProxyCheckEvent
     */
    public function createProxyCheckEvent()
    {
        $proxy = $this->createProxy();
        $url = $this->createUrl();
        return new class($proxy, $url) extends AbstractProxyCheckEvent
        {
            protected function beforeRunCallbacks()
            {
                // TODO: Implement beforeRunCallbacks() method.
            }
        };
    }

    public function testCreateEvent()
    {
        $event = $this->createProxyCheckEvent();
        $this->assertEquals(false, $event->isHappened());

        $this->expectException(\TypeError::class);
        $event->getDuration();

        $this->expectException(\Exception::class);
        $event->getResult();
    }

    public function testProxyCheckEventHandleSetResult()
    {
        $event = $this->createProxyCheckEvent();
        $event->setResult(ResultFactory::createSuccess());
        $this->assertEquals(true, $event->isHappened());
        $this->assertEquals(true, $event->getResult()->isSuccess());

        $event = $this->createProxyCheckEvent();
        $event->setResult(ResultFactory::createFail(new \Exception()));
        $this->assertEquals(true, $event->isHappened());
        $this->assertEquals(false, $event->getResult()->isSuccess());
    }

    /**
     * @param AbstractProxyCheckEvent $event
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function createRequest(AbstractProxyCheckEvent $event)
    {
        $client = new Client(['verify' => false]);
        return RequestFactory::createRequestForEvent($client, $event);
    }

    public function testSimpleRequest()
    {
        $event = $this->createProxyCheckEvent();
        $promise = $this->createRequest($event);
        $promise->wait(false);
        $this->assertEquals(true, $event->isHappened());
        $this->assertInternalType('int', $event->getDuration());
    }

    public function testSimpleRequestWithLogResult()
    {
        $event = $this->createProxyCheckEvent();
        $promise = $this->createRequest($event);
        $this->expectOutputRegex('/[FAIL|SUCCESS].+?/');
        $event->addCallback(function (AbstractProxyCheckEvent $event) {
            echo (new ProxyCheckEventResultFormatter())->format($event);
        });
        $promise->wait(false);
    }
}
