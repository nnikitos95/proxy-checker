<?php

use App\models\adapter\ProxyEntityToProxyEntityCheckContextAdapter;
use App\models\adapter\RowToProxyEntityAdapter;
use App\models\checker\ProxyChecker;
use App\models\event\AbstractProxyCheckEvent;
use App\models\formatter\ProxyEntityCheckEventResultFormatter;
use App\models\proxy\ProxyEntity;
use App\models\storage\UrlStorage;

$orm = \App\Registry::getORM();

$states = [
    ProxyEntity::BAD,
    ProxyEntity::REPEAT,
    ProxyEntity::GOOD
];

$uriList = \App\Config::getOption('links');
$urlStorage = new UrlStorage($uriList);

$proxyList = [];

while (true) {
    $proxyList = ProxyEntity::getByOrm()
        ->where(['quality' => $states])
        ->where('timeout < 30')
        ->orderBy('lastCheckTime')
        ->limit(50)
        ->fetchAll()
    ;

    if (empty($proxyList)) {
        sleep(60);
        continue;
    }

    $proxyList = (new RowToProxyEntityAdapter())->getMany($proxyList);
    $proxyList = (new ProxyEntityToProxyEntityCheckContextAdapter())
        ->getMany($proxyList, [
                function (AbstractProxyCheckEvent $event) {
                    $message = (new ProxyEntityCheckEventResultFormatter())
                        ->format($event)
                    ;
                    \App\Registry::getConsoleLogger()
                        ->info($message)
                    ;
                }
            ]
        );
    (new ProxyChecker($proxyList, $urlStorage))->check();

    $proxyList = [];
}