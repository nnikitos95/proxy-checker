<?php

use App\models\adapter\RowToProxyEntityAdapter;
use App\models\event\AbstractProxyCheckEvent;
use App\models\formatter\ProxyEntityCheckEventResultFormatter;
use App\models\proxy\ProxyEntity;
use App\models\url\Url;
use App\RequestFactory;
use GuzzleHttp\Client;


// Create Url
$url = new Url('https://vl.ru');

//Get one proxy from database
$proxy = ProxyEntity::getByOrm()
    ->where(['quality' => ProxyEntity::GOOD])
    ->fetch()
;

//Adapt proxy from database to ProxyEntity
$proxy = (new RowToProxyEntityAdapter())->getOne($proxy);

//Create event
$event = new \App\models\event\ProxyEntityCheckEvent($proxy, $url);
$event->addCallback(function (AbstractProxyCheckEvent $event) {
    \App\Registry::getConsoleLogger()->info(
        (new ProxyEntityCheckEventResultFormatter())->format($event)
    );
});

$client = new Client(['verify' => false]);

$promise = RequestFactory::createRequestForEvent($client, $event);

$promise->wait(false);