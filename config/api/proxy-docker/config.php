<?php

return [
    'connection' => [
        'host' => 'https://www.proxydocker.com/en/proxylist/api',
        'email' => 'girfanov@vl.ru'
    ],
    'params' => [
        'countries' => [
            'RUSSIA',
            'GERMANY',
            'NETHERLANDS',
            'JAPAN',
            'AUSTRIA',
            'BELARUS',
            'BELGIUM',
            'ESTONIA',
            'FINLAND',
            'FRANCE',
            'KAZAKHSTAN',
            'LATVIA',
            'NORWAY',
            'LITHUANIA',
            'MOLDOVA',
            'POLAND',
            'SLOVAKIA',
            'SLOVENIA',
            'SWEDEN',
            'SWITZERLAND',
            'United Kingdom',
            'United States',
            'Singapore',
            'Indonesia',
            'Italy',
            'Brazil',
            'Canada',
            'Afghanistan'
        ],
        'needs' => [
            'Google',
            'BOT',
            'SEO',
        ],
        'types' => [
            'HTTP',
            'HTTPS'
        ],
        'anonymities' => [
            'ELITE',
            'ANONYMOUS'
        ],
    ],
];