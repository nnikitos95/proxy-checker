<?php

define('APP_PATH', __DIR__);

require 'vendor/autoload.php';

$env = 'production';

if (file_exists(APP_PATH . '/config/env')) {
    $env = trim(file_get_contents(APP_PATH . '/config/env'));
}

$config = new Zend_Config_Ini(APP_PATH . '/config/application.ini', $env);
\App\Config::init($config->toArray());

$router = new \MiladRahimi\PHPRouter\Router();

$router->get('/list/?.+?' ,
    'App\controllers\ApiController@listMethod'
);
$router->post('/bad',
    'App\controllers\ApiController@badMethod'
);

try {
    $router->dispatch();
} catch(Exception $e) {
    //TODO сделать нормально
    header('Something Wrong', true, 404);
    echo $e->getMessage();
}

exit();